package beweb.montpellier.poo.entites.entites_non_vivantes;

class Leg {

    /**
     * PROPERTY
     */
    private double sizeOfLeg;
    
    /**
     * CONSTRUCTOR
     */
    public Leg() {
        this.sizeOfLeg = 0;
    }

    public Leg(double sizeOfLeg) {
        this.sizeOfLeg = sizeOfLeg;
    }

    /**
     * ACCESSORS
     */
    public double getSizeOfLeg() {
        return sizeOfLeg;
    }

    public void setSizeOfLeg(double sizeOfLeg) {
        this.sizeOfLeg = sizeOfLeg;
    }
}