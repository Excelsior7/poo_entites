package beweb.montpellier.poo.entites.entites_non_vivantes;

import java.util.Scanner;

/**
 * FournitureFactory
 */
public class FournitureFactory {

    private static String name;
    private static String color;
    private static Integer nbrOfLegs;
    private static double sizeForTheLegs;

    public static PieceOfFourniture getPieceOfFourniture(String fourniture) {

        final Scanner scanner = new Scanner(System.in);
        
        while (!fourniture.equals("Table") && !fourniture.equals("Chair") && !fourniture.equals("Cabinet") && !fourniture.equals("Buffet") && !fourniture.equals("Bench")) {
            System.out.println("We don't understand your wishes : ");
            System.out.println("What piece of fourniture you want ?");
            System.out.println("\n");

            System.out.println("* Table ");
            System.out.println("* Chair ");
            System.out.println("* Cabinet ");
            System.out.println("* Buffet ");
            System.out.println("* Buffet ");
            System.out.println("\n");

            fourniture = scanner.nextLine();
        }

        name = CustomizeFourniture.customName();
        color = CustomizeFourniture.customColor();
        nbrOfLegs = CustomizeFourniture.customNumberOfLegs();
        sizeForTheLegs = CustomizeFourniture.customSizeOfLegs();

        scanner.close();

        switch(fourniture) {
            case "Table" :
                return new Table(name, color, nbrOfLegs, sizeForTheLegs);
            case "Chair" :
                return new Chair(name, color, nbrOfLegs, sizeForTheLegs);
            case "Cabinet" :
                return new Cabinet(name, color, nbrOfLegs, sizeForTheLegs);
            case "Buffet" :
                return new Buffet(name, color, nbrOfLegs, sizeForTheLegs);   
            case "Bench" :
                return new Bench(name, color, nbrOfLegs, sizeForTheLegs);  
            default :
                return null;
        }
    }
}

