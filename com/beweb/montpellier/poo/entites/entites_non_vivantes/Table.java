package beweb.montpellier.poo.entites.entites_non_vivantes;

public class Table extends PieceOfFourniture {

    /**
     * CONSTRUCTOR
     */
    public Table() {
        super();
    }

    public Table(Matter matter) {
        super(matter);
    }

    public Table(String color, String name, Integer numbersOfLegs, double sizeOfTheLegs) {
        super(color, name, numbersOfLegs, sizeOfTheLegs);
    }
    
    /**
     * METHOD
     */
    @Override
    public String content() {
        StringBuilder sb = new StringBuilder();
        
        sb.append("On my base there is :");
        sb.append("\n");

        sb.append("4 plates");
        sb.append("\n");

        sb.append("4 forks and 4 knives");
        sb.append("\n");

        sb.append("Water, Soda... And some foods");
        sb.append("\n");

        return sb.toString();
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("** TABLE **");
        sb.append("\n");

        sb.append("Matter : "+this.matter.getName());
        sb.append("\n");

        sb.append("Color of the matter: "+ this.matter.getColor());
        sb.append("\n");

        if(this.getLegsOfTheTable().size() == 0) {
            sb.append("Number of leg : 0 ");
            sb.append("\n");
        }
        else if(this.getLegsOfTheTable().size() == 1) {
            sb.append("Number of leg : "+this.getLegsOfTheTable().size());
            sb.append("\n");

            sb.append("Size of the leg : "+this.getLegsOfTheTable().get(0).getSizeOfLeg());
            sb.append("\n");
        }
        else {
            sb.append("Number of legs : "+this.getLegsOfTheTable().size());
            sb.append("\n");

            sb.append("Size of one leg : "+this.getLegsOfTheTable().get(0).getSizeOfLeg());
            sb.append("\n");
        }

        return sb.toString();
    }
}