package beweb.montpellier.poo.entites.entites_non_vivantes;

import java.util.Scanner;

/**
 * ConstructFourniture
 */
public class CustomizeFourniture {

    static Scanner scanner = new Scanner(System.in);
    private static String name;
    private static String color;
    private static Integer nbrOfLegs;
    private static double sizeForTheLegs;

    public static String customName() {

        System.out.println("What matter do you want ?");
        name = scanner.nextLine();

        return name;
    }

    public static String customColor() {

        System.out.println("What color do you want for your matter?");
        color = scanner.nextLine();

        return color;
    }

    public static Integer customNumberOfLegs() {

        System.out.println("How many legs do you want?");
        nbrOfLegs = scanner.nextInt();

        return nbrOfLegs;
    }

    public static double customSizeOfLegs() {


        System.out.println("What is the size for your Legs");
        sizeForTheLegs = scanner.nextDouble();

        return sizeForTheLegs;
    }
}
