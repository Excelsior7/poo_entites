package beweb.montpellier.poo.entites.entites_non_vivantes;

class Matter {
    
    /**
     * PROPERTIES
     */
    private String color;
    private String name;

    /**
     * CONSTRUCTOR
     */
    public Matter() {
        this.color = "";
        this.name = "";
    }

    public Matter(String color, String name) {
        this.color = color;
        this.name = name;
    }


    /**
     * ACCESSORS
     */
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
}