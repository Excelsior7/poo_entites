package beweb.montpellier.poo.entites.entites_non_vivantes;

public class Chair extends PieceOfFourniture {

    /**
     * CONSTRUCTOR
     */
    public Chair() {
        super();
    }

    public Chair(Matter matter) {
        super(matter);
    }

    public Chair(String color, String name, Integer numbersOfLegs, double sizeOfTheLegs) {
        super(color, name, numbersOfLegs, sizeOfTheLegs);
    }
    
    /**
     * METHOD
     */
    
    @Override
    public String content() {
        return "Person on the chair";
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("** CHAIR **");
        sb.append("\n");

        sb.append("Matter : "+this.matter.getName());
        sb.append("\n");

        sb.append("Color of the matter: "+ this.matter.getColor());
        sb.append("\n");

        if(this.getLegsOfTheTable().size() == 0) {
            sb.append("Number of leg : 0 ");
            sb.append("\n");
        }
        else if(this.getLegsOfTheTable().size() == 1) {
            sb.append("Number of leg : "+this.getLegsOfTheTable().size());
            sb.append("\n");

            sb.append("Size of the leg : "+this.getLegsOfTheTable().get(0).getSizeOfLeg());
            sb.append("\n");
        }
        else {
            sb.append("Number of legs : "+this.getLegsOfTheTable().size());
            sb.append("\n");

            sb.append("Size of one leg : "+this.getLegsOfTheTable().get(0).getSizeOfLeg());
            sb.append("\n");
        }

        return sb.toString();
    }
}