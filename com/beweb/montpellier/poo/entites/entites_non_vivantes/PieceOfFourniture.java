package beweb.montpellier.poo.entites.entites_non_vivantes;

import java.util.ArrayList;
import java.util.List;

public abstract class PieceOfFourniture {
    
    /**
     * PROPERTIES
     */
    protected Matter matter;
    protected List<Leg> legsOfTheTable;

    /**
     * CONSTRUCTOR
     */

     //WITHOUT MATTER OR LEG
    public PieceOfFourniture() {
        this.matter = new Matter();
        legsOfTheTable = new ArrayList<>();
    }
    
    //WITH MATTER ONLY
    public PieceOfFourniture(Matter matter) {
        this.matter = matter;
        this.legsOfTheTable = new ArrayList<>();
    }

    //WITH MATTER AND LEG(S)
    public PieceOfFourniture(String color, String name, Integer numbersOfLegs, double sizeOfTheLegs) {
        this.matter = new Matter(color, name);
        this.legsOfTheTable = new ArrayList<>();
        this.addLegsInTheTable(numbersOfLegs, sizeOfTheLegs);
    }
    
    /**
     * ACCESSORS
     */
    public Matter getMatter() {
        return matter;
    }

    public void setMatter(Matter matter) {
        this.matter = matter;
    }

    public List<Leg> getLegsOfTheTable() {
        return legsOfTheTable;
    }

    public void addLegsInTheTable(Integer numbersOfLegs, double sizeOfTheLegs) {
        
        for(int i = 0; i < numbersOfLegs; i++)
            this.legsOfTheTable.add(new Leg(sizeOfTheLegs));
    }

    /**
     * METHODS
     */
    
    public abstract String content();
}
