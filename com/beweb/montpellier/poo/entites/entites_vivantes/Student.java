import beweb.montpellier.poo.entites.entites_non_vivantes.PieceOfFourniture;

public class Student extends Person {

    /**
     * CONSTRUCTOR
     */
    public Student() {
        super();
    }

    public Student(String firstName, String lastName) {
        super(firstName, lastName);
    }

    @Override
    public String observe(PieceOfFourniture pof) {
        return pof.content();
    }
}
