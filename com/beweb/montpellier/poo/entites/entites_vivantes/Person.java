import beweb.montpellier.poo.entites.entites_non_vivantes.PieceOfFourniture;

public abstract class Person {

    /**
     * PROPERTIES
     */
    protected String firstName;
    protected String lastName;

    /**
     * CONSTRUCTOR
     */
    protected Person() {
        this.firstName = "";
        this.lastName = "";
    }
    
    protected Person(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    /**
     * ACCESSORS
     */

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * METHODS
     */

    public abstract String observe(PieceOfFourniture pof);

    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append(" ~ PRESENTATION ~ ");
        sb.append("\n");
        sb.append("\n");

        sb.append("FIRSTNAME : "+this.getFirstName());
        sb.append("\n");

        sb.append("LASTNAME : "+this.getLastName());
        sb.append("\n");

        return sb.toString();
    }
}

