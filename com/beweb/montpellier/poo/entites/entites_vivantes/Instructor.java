import beweb.montpellier.poo.entites.entites_non_vivantes.PieceOfFourniture;

/**
 * Instructor
 */
public class Instructor extends Person {

    /**
     * CONSTRUCTOR
     */
    public Instructor() {
    }

    public Instructor(String firstName, String lastName) {
        super(firstName, lastName);
    }

    /**
     * METHODS
     */
    @Override
    public String observe(PieceOfFourniture pof) {
        return pof.content();
    }
    
    //ADD GRADES TO A TRAINEE
    public void addNotesToTrainee(Trainee trainee, double note) {
        trainee.addNotes(note);
    }   

    //COMPUTE MEAN OF ONE'S TRAINEE GRADE 
    public void computeMeanOfTraineesGrades(Trainee trainee) {
        double sum = 0;

        for(int i = 0; i < trainee.getListNotes().size(); i++) 
            sum += trainee.getListNotes().get(i);
        
        displayMean(sum / trainee.getListNotes().size());
    }

    public static void displayMean(double mean) {
        if(mean >= 10) 
            System.out.println("GOOD CONTINUE : "+mean);
        else 
            System.err.println("BAD ... CAN DO BETTER : "+mean);
    }
}
