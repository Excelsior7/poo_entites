import java.util.List;

import beweb.montpellier.poo.entites.entites_non_vivantes.PieceOfFourniture;

/**
 * Director
 */
public class Director extends Person {

    private List<Trainee> listOfTrainees;

    /**
     * CONSTRUCTOR
     */
    public Director() {

    }

    public Director(String firstName, String lastName) {
        super(firstName, lastName);
    }

    /**
     * ACCESSORS
     */
    public List<Trainee> getListOfTrainees() {
        return listOfTrainees;
    }

    public void setListOfTrainees(List<Trainee> listOfTrainees) {
        this.listOfTrainees = listOfTrainees;
    }

    /**
     * METHODS
     */
    @Override
    public String observe(PieceOfFourniture pof) {
        return pof.content();
    }

    //COMPUTE MEAN OF EVERY TRAINEES OF THE SCHOOL
    public void computeMeanOfEveryTrainee(List<Trainee> listOfTrainees) {
        double sum = 0;
        int numberOfGrades = 0;

        for(Trainee trainee : listOfTrainees) {
            for(int i = 0; i < trainee.getListNotes().size(); i++) {
                sum += trainee.getListNotes().get(i);
                numberOfGrades++;
            }
        }

        displayMean(sum / numberOfGrades);
    }

    public static void displayMean(double mean) {
        if(mean >= 10) 
            System.out.println("GOOD CONTINUE : "+mean);
        else 
            System.err.println("BAD ... CAN DO BETTER : "+mean);
    }
}

