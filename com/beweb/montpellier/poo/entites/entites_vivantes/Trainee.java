import java.util.ArrayList;
import java.util.List;

import beweb.montpellier.poo.entites.entites_non_vivantes.PieceOfFourniture;

/**
 * Trainee
 */
public class Trainee extends Person {

    protected List<Double> listNotes;

    /**
     * CONSTRUCTOR
     */
    public Trainee() {
        super();
        listNotes = new ArrayList<>();
    }
    
    public Trainee(String firstName, String lastName) {
        super(firstName, lastName);
        listNotes = new ArrayList<>();
    }

    /**
     * ACCESSORS
     */    
    public List<Double> getListNotes() {
        return listNotes;
    }

    public void setListNotes(List<Double> listNotes) {
        this.listNotes = listNotes;
    }

    /**
     * METHODS
     */
    @Override
    public String observe(PieceOfFourniture pof) {
        return pof.content();
    }

    //ADD GRADE TO A TRAINEE
    public void addNotes(Double grade) {
        this.listNotes.add(grade);
    }
}

