import java.util.ArrayList;
import java.util.List;

// import beweb.montpellier.poo.entites.entites_non_vivantes.PieceOfFourniture;
// import beweb.montpellier.poo.entites.entites_non_vivantes.Table;

public class Main {

    public static void main(final String[] args) {
        List<Trainee> listOfTrainees = new ArrayList<>();
        final Instructor instructor = new Instructor("Richard", "Feynman");
        final Director captain = new Director("Captain", "Train");

        //INSTANCIATE TRAINEES

        final Trainee trainee = new Trainee("Self", "Made");
        final Trainee trainee1 = new Trainee("Tiger", "Made");
        final Trainee trainee2 = new Trainee("Lion", "Made");
        final Trainee trainee3 = new Trainee("Monkey", "Made");
        final Trainee trainee4 = new Trainee("Billio", "Made");
        final Trainee trainee5 = new Trainee("CEO", "Made");

        //ADD TRAINEES TO SCHOOL

        try {
            listOfTrainees.add(trainee);
            listOfTrainees.add(trainee1);
            listOfTrainees.add(trainee2);
            listOfTrainees.add(trainee3);
            listOfTrainees.add(trainee4);
            listOfTrainees.add(trainee5);

        } catch (IllegalArgumentException e) {
            e.getMessage();
        }

        //DEFINE GRADES TO TRAINEES

        for(Trainee trainees : listOfTrainees) {
            instructor.addNotesToTrainee(trainees, 19);
            instructor.addNotesToTrainee(trainees, 17.5);
            instructor.addNotesToTrainee(trainees, 20);
            instructor.addNotesToTrainee(trainees, 18.5);
        }

        //INSTRUCTOR COMPUTE MEAN OF ONE TRAINEE
        instructor.computeMeanOfTraineesGrades(trainee);

        //ADD EVERY TRAINEES IN THE SCHOOL OF THE DIRECTOR
        captain.setListOfTrainees(listOfTrainees);

        //DIRECTOR CAPTAIN COMPUTE MEAN OF ALL NOTES OF ALL TRAINEES 
        captain.computeMeanOfEveryTrainee(listOfTrainees);

    }
}


